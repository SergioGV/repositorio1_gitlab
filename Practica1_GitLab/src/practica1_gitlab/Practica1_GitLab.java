/*
 * Entornos de Desarrollo
 * Practica1 GitLab
 * 09/11/2021 Sergio González Vélez
 */
package practica1_gitlab;


public class Practica1_GitLab {

    
    public static void main(String[] args) {
        System.out.println("Mi primer commit desde NetBeans");
        System.out.println("Commit desde GitLab");
        System.out.println("Nuevo cambio");
        System.out.println("Cuarto commit");
    }
    
}
